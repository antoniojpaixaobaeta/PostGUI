﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PostGUI
{
    class Post
    {
        #region Atributos

        private string _titulo;

        private string _descricao;

        private DateTime _data;

        private int _gosto;

        private int _naogosto;

        #endregion

        #region Propriedades

        public string Titulo
        {
            get
            {
                return _titulo;
            }

            set
            {
                _titulo = value;
            }
        }

        public string Descricao
        {
            get
            {
                return _descricao;
            }

            set
            {
                _descricao = value;
            }
        }

        public DateTime Data
        {
            get
            {
                return _data;
            }

            set
            {
                _data = value;
            }
        }

        public int Gosto
        {
            get
            {
                return _gosto;
            }

            set
            {
                _gosto = value;
            }
        }

        public int Naogosto
        {
            get
            {
                return _naogosto;
            }

            set
            {
                _naogosto = value;
            }
        }

        #endregion

        #region Construtores

        public Post()
        {
            Titulo = "??";
            Descricao = "??";
            Data = DateTime.Now;
            Gosto = 0;
            Naogosto = 0;

        }
        


        #endregion

        #region Métodos

        public void Gostar()

        {

            Gosto++;


        }

        public void NaoGostar()

        {
            Gosto--;
        }


        #endregion



    }
}
