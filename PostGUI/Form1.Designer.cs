﻿namespace PostGUI
{
    partial class Form1
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Datalabel1 = new System.Windows.Forms.Label();
            this.ComentarioLabel = new System.Windows.Forms.Label();
            this.GostoButton = new System.Windows.Forms.Button();
            this.NaoGostoButton = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Datalabel1
            // 
            this.Datalabel1.AutoSize = true;
            this.Datalabel1.Location = new System.Drawing.Point(65, 60);
            this.Datalabel1.Name = "Datalabel1";
            this.Datalabel1.Size = new System.Drawing.Size(30, 13);
            this.Datalabel1.TabIndex = 0;
            this.Datalabel1.Text = "Data";
            // 
            // ComentarioLabel
            // 
            this.ComentarioLabel.AutoSize = true;
            this.ComentarioLabel.Location = new System.Drawing.Point(136, 86);
            this.ComentarioLabel.Name = "ComentarioLabel";
            this.ComentarioLabel.Size = new System.Drawing.Size(60, 13);
            this.ComentarioLabel.TabIndex = 1;
            this.ComentarioLabel.Text = "Comentario";
            // 
            // GostoButton
            // 
            this.GostoButton.Location = new System.Drawing.Point(68, 211);
            this.GostoButton.Name = "GostoButton";
            this.GostoButton.Size = new System.Drawing.Size(75, 23);
            this.GostoButton.TabIndex = 2;
            this.GostoButton.Text = "Gosto";
            this.GostoButton.UseVisualStyleBackColor = true;
            // 
            // NaoGostoButton
            // 
            this.NaoGostoButton.Location = new System.Drawing.Point(210, 211);
            this.NaoGostoButton.Name = "NaoGostoButton";
            this.NaoGostoButton.Size = new System.Drawing.Size(75, 23);
            this.NaoGostoButton.TabIndex = 3;
            this.NaoGostoButton.Text = "Não Gosto";
            this.NaoGostoButton.UseVisualStyleBackColor = true;
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(380, 335);
            this.Controls.Add(this.NaoGostoButton);
            this.Controls.Add(this.GostoButton);
            this.Controls.Add(this.ComentarioLabel);
            this.Controls.Add(this.Datalabel1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label Datalabel1;
        private System.Windows.Forms.Label ComentarioLabel;
        private System.Windows.Forms.Button GostoButton;
        private System.Windows.Forms.Button NaoGostoButton;
    }
}

